package com.xbd.marsh;

import org.noear.solon.Solon;
import org.noear.solon.annotation.SolonMain;

@SolonMain
public class MarshApp {

    public static void main(String[] args) {
        Solon.start(MarshApp.class, args);
    }
}
