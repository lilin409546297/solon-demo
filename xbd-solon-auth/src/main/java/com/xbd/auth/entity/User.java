package com.xbd.auth.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("auth_user")
public class User extends BaseEntity{

    private String username;

    private String password;

}
