package com.xbd.auth.exception;

import org.noear.solon.core.handle.Result;
import org.noear.solon.core.util.DataThrowable;

public class ApiException extends DataThrowable {

    public ApiException() {
        super.data(Result.failure());
    }

    public ApiException(Throwable cause) {
        super(cause);
        super.data(Result.failure(cause.getMessage()));
    }

    public ApiException(String message) {
        super(message);
        super.data(Result.failure(message));
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
        super.data(Result.failure(message));
    }
}
