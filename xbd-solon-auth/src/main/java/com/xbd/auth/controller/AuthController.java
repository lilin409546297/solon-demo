package com.xbd.auth.controller;

import com.alibaba.fastjson.JSONObject;
import com.xbd.auth.service.IUserService;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Result;

@Controller
@Mapping("/auth")
public class AuthController {

    @Inject
    private IUserService userService;

    @Get
    @Mapping("/test")
    public Result test(Context context) {
        return Result.succeed(context.session("userId"));
    }

    @Post
    @Mapping("/login")
    public Result login(Context context, @Body JSONObject jsonObject) {
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        boolean result = userService.checkUser(context, username, password);
        return result ? Result.succeed() : Result.failure("密码错误！");
    }
}
