package com.xbd.auth.init;

import cn.hutool.crypto.digest.DigestUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xbd.auth.entity.User;
import com.xbd.auth.mapper.UserMapper;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.annotation.Init;
import org.noear.solon.annotation.ProxyComponent;

@ProxyComponent
public class UserInit{

    @Db
    private UserMapper userMapper;


    @Init
    private void init() {
        System.out.println("init..........init");
        Long count = userMapper.selectCount(new QueryWrapper<>());
        if (count == 0) {
            User user = new User();
            user.setUsername("admin");
            user.setPassword(DigestUtil.bcrypt("admin"));
            userMapper.insert(user);
        }
    }
}
