package com.xbd.auth;

import org.noear.solon.Solon;
import org.noear.solon.annotation.SolonMain;
import org.noear.solon.sessionstate.jwt.JwtUtils;

@SolonMain
public class AuthApp {

    public static void main(String[] args) {
        System.out.println(JwtUtils.createKey());;
        Solon.start(AuthApp.class, args);
    }
}
