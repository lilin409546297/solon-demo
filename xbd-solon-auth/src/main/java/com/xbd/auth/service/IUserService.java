package com.xbd.auth.service;

import com.baomidou.mybatisplus.solon.service.IService;
import com.xbd.auth.entity.User;
import org.noear.solon.core.handle.Context;

public interface IUserService extends IService<User> {

    boolean checkUser(Context context, String username, String password);
}
