package com.xbd.auth.config;

import com.zaxxer.hikari.HikariDataSource;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

    @Bean(name = "default", typed = true)
    public DataSource dataSource(@Inject("${datasource.default}") HikariDataSource dataSource){
        return dataSource;
    }
}
