package com.xbd.auth.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;

import java.time.Instant;

public class XbdMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createDate", Instant.now().toEpochMilli(), metaObject);
        this.setFieldValByName("modifyDate", Instant.now().toEpochMilli(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("modifyDate", Instant.now().toEpochMilli(), metaObject);
    }


}
