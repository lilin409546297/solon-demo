package com.xbd.gateway;

import org.noear.solon.Solon;
import org.noear.solon.annotation.SolonMain;

@SolonMain
public class GatewayApp {

    public static void main(String[] args) {
        Solon.start(GatewayApp.class, args);
    }
}
