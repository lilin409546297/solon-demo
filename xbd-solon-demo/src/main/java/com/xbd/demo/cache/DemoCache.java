package com.xbd.demo.cache;

import org.noear.solon.annotation.Param;
import org.noear.solon.annotation.ProxyComponent;
import org.noear.solon.data.annotation.Cache;

@ProxyComponent
public class DemoCache {

    @Cache(key = "demo:${name}", seconds = 600)
    public String get(@Param("name") String name) {
        System.out.println("get.............get");
        return "name==" + name;
    }
}
