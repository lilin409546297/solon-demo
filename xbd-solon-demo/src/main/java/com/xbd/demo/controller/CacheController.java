package com.xbd.demo.controller;

import com.xbd.demo.cache.DemoCache;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Result;

@Controller
@Mapping("/cache")
public class CacheController {

    @Inject
    private DemoCache demoCache;

    @Get
    @Mapping("/test/{name}")
    public Result test(@Path("name") String name) {
        return Result.succeed(demoCache.get(name));
    }

}
