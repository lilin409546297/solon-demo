package com.xbd.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.xbd.demo.common.result.ResponseMsg;
import com.xbd.demo.common.service.IUserService;
import com.xbd.demo.feign.UserFeign;
import org.noear.nami.annotation.NamiClient;
import org.noear.nami.common.ContentTypes;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;

import java.util.List;

@Controller
@Mapping("/demo")
public class DemoController {

    @Inject("${solon.app.name}")
    private String appName;

    @NamiClient(name = "demo", path = "/user", headers = ContentTypes.PROTOBUF)
    private IUserService userService;

    @Inject
    private UserFeign userFeign;

    @Get
    @Mapping("/demo")
    public ResponseMsg demo() {
        return ResponseMsg.success(appName);
    }

    @Get
    @Mapping("/rpc")
    public ResponseMsg rpc() {
        System.out.println(System.currentTimeMillis());
        List<String> users = userService.listUser();
        System.out.println(System.currentTimeMillis());
        return ResponseMsg.success(users);
    }

    @Get
    @Mapping("/feign")
    public ResponseMsg feign() {
        System.out.println(System.currentTimeMillis());
        String msg = userFeign.demo();
        System.out.println(System.currentTimeMillis());
        return JSONObject.parseObject(msg, ResponseMsg.class);

    }

}
