package com.xbd.demo.config;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import org.noear.solon.cache.redisson.RedissonCacheService;
import org.noear.solon.data.cache.CacheService;

@Configuration
public class RedisConfiguration {

    @Bean(name = "default", typed = true)
    public CacheService cacheService(@Inject("${redis.default}") RedissonCacheService cacheReactive) {
        return cacheReactive;
    }
}
