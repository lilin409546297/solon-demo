package com.xbd.demo.config;

import com.alibaba.fastjson.JSONObject;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.auth.AuthAdapter;
import org.noear.solon.auth.AuthProcessor;
import org.noear.solon.auth.annotation.Logical;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.ContextUtil;

@Configuration
public class AuthConfiguration {

    @Bean
    public AuthAdapter authAdapter() {
        return new AuthAdapter()
//            .loginUrl("/login") //设定登录地址，未登录时自动跳转（如果不设定，则输出401错误）
            .addRule(r -> r.include("/**").verifyLogined()) //添加规则
            .processor(new AuthProcessor() {
                @Override
                public boolean verifyIp(String ip) {
                    return false;
                }

                @Override
                public boolean verifyLogined() {
                    Context context = ContextUtil.current();
                    if (context.session("userId") != null) {
                        return true;
                    }
                    return false;
                }

                @Override
                public boolean verifyPath(String path, String method) {
                    return false;
                }

                @Override
                public boolean verifyPermissions(String[] permissions, Logical logical) {
                    return false;
                }

                @Override
                public boolean verifyRoles(String[] roles, Logical logical) {
                    return false;
                }
            }).failure((ctx, rst) -> {
                System.out.println(rst);
                ctx.output(JSONObject.toJSONString(rst));
            });
    }

}
