package com.xbd.demo.feign;

import com.alibaba.fastjson.JSONObject;
import com.xbd.demo.common.result.ResponseMsg;
import feign.RequestLine;
import feign.solon.FeignClient;
import org.noear.solon.annotation.Get;

@FeignClient(name = "demo", path = "/demo")
public interface UserFeign {

    @Get
    @RequestLine("GET /demo")
    String demo();
}
