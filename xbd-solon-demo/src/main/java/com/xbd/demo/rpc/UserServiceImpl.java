package com.xbd.demo.rpc;

import com.xbd.demo.common.service.IUserService;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Remoting;
import org.noear.solon.aspect.annotation.Service;
import org.noear.solon.data.annotation.Tran;

import java.util.ArrayList;
import java.util.List;

@Service
@Mapping("/user")
@Remoting
public class UserServiceImpl implements IUserService {

    @Tran
    public List<String> listUser() {
        List<String> users = new ArrayList<String>();
        for (int i = 0; i < 5; i++) {
            users.add("user" + i);
        }
        return users;
    }
}
