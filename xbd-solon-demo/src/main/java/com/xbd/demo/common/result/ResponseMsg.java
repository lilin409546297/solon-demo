package com.xbd.demo.common.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMsg<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Boolean status;
    private T body;
    private String msg;

    public static ResponseMsg success() {
        return ResponseMsg
                .builder()
                .status(true)
                .build();
    }

    public static ResponseMsg failure() {
        return ResponseMsg
                .builder()
                .status(false)
                .build();
    }

    public static <T> ResponseMsg<T> success(T body) {
        return ResponseMsg
                .<T>builder()
                .status(true)
                .body(body)
                .build();
    }

    public static ResponseMsg failure(String msg) {
        return ResponseMsg
                .builder()
                .status(false)
                .msg(msg)
                .build();
    }
}
