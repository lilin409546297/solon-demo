package com.xbd.demo;

import feign.solon.EnableFeignClient;
import org.noear.solon.Solon;
import org.noear.solon.annotation.SolonMain;

@SolonMain
@EnableFeignClient
public class DemoApp {

    public static void main(String[] args) {
        Solon.start(DemoApp.class, args);
    }
}
